/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  TouchableOpacityComponent,
} from 'react-native';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text>Laboratorio 2</Text>
        </View>
        <Image style={styles.image} source={require('./img/react.png')} />
        <TextInput
          style={styles.input}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />
        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text>Touch here</Text>
        </TouchableOpacity>
        <View style={[styles.countContainer]}>
          <Text style={[styles.countText]}>{this.state.count}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  image: {
    width: 100,
    height: 100,
  },
  input: {
    margin: 20,
    width: 200,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});

export default App;
